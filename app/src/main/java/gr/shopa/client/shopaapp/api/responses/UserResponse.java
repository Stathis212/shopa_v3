package gr.shopa.client.shopaapp.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stathis on 16-Feb-18.
 */

public class UserResponse {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("fullName")
    @Expose
    private Object fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("emailConfirmed")
    @Expose
    private Boolean emailConfirmed;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("joinDate")
    @Expose
    private String joinDate;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("profiles")
    @Expose
    private Object profiles;
    @SerializedName("shops")
    @Expose
    private Object shops;
    @SerializedName("points")
    @Expose
    private Object points;
    @SerializedName("claims")
    @Expose
    private List<Object> claims = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailConfirmed() {
        return emailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Object getProfiles() {
        return profiles;
    }

    public void setProfiles(Object profiles) {
        this.profiles = profiles;
    }

    public Object getShops() {
        return shops;
    }

    public void setShops(Object shops) {
        this.shops = shops;
    }

    public Object getPoints() {
        return points;
    }

    public void setPoints(Object points) {
        this.points = points;
    }

    public List<Object> getClaims() {
        return claims;
    }

    public void setClaims(List<Object> claims) {
        this.claims = claims;
    }

}
