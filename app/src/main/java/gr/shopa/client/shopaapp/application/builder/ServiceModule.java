package gr.shopa.client.shopaapp.application.builder;

import dagger.Module;
import dagger.Provides;
import gr.shopa.client.shopaapp.api.ShopaAPI;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Module
public class ServiceModule {
    private static final String BASE_URL = "http://shopaapi.azurewebsites.net/";

    @ShopaScope
    @Provides
    ShopaAPI provideApiService(OkHttpClient client, GsonConverterFactory gson, RxJavaCallAdapterFactory rxAdapter)
    {
        Retrofit retrofit =   new Retrofit.Builder().client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(gson)
                .addCallAdapterFactory(rxAdapter).build();

        return  retrofit.create(ShopaAPI.class);
    }
}
