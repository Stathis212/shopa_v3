package gr.shopa.client.shopaapp.screens.base.dagger;

import dagger.Component;
import gr.shopa.client.shopaapp.application.builder.ShopaComponent;
import gr.shopa.client.shopaapp.screens.base.BaseActivity;

/**
 * Created by Stathis on 03-Mar-18.
 */

@BaseScope
@Component(modules = {BaseModule.class}, dependencies = {ShopaComponent.class})
public interface BaseComponent {
    void inject(BaseActivity activity);
}
