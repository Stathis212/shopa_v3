package gr.shopa.client.shopaapp.screens.base.core;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import butterknife.ButterKnife;
import gr.shopa.client.shopaapp.R;
import gr.shopa.client.shopaapp.screens.base.BaseActivity;

/**
 * Created by Stathis on 04-Mar-18.
 */

public class BaseView {

    private View view;

    public BaseView(BaseActivity context) {
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_base, parent, true);
        ButterKnife.bind(view, context);
    }

    public View constructView() {
        return view;
    }

}
