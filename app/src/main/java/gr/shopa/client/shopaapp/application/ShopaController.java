package gr.shopa.client.shopaapp.application;

import android.app.Application;

import com.squareup.leakcanary.BuildConfig;
import com.squareup.leakcanary.LeakCanary;

import gr.shopa.client.shopaapp.application.builder.DaggerShopaComponent;
import gr.shopa.client.shopaapp.application.builder.ShopaComponent;
import gr.shopa.client.shopaapp.application.builder.ShopaModule;
import timber.log.Timber;

/**
 * Created by Stathis on 03-Mar-18.
 */

public class ShopaController extends Application{

    private static ShopaComponent shopaComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initialiseLogger();
        initAppComponent();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...

    }

    private void initAppComponent() {
        shopaComponent = DaggerShopaComponent.builder()
                .shopaModule(new ShopaModule(this))
                .build();
    }

    private void initialiseLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new Timber.Tree() {
                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    //TODO  decide what to log in release version
                }
            });
        }
    }

    public static ShopaComponent shopaComponent() {
        return shopaComponent;
    }
}
