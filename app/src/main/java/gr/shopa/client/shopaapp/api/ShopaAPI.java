package gr.shopa.client.shopaapp.api;

import java.util.List;

import gr.shopa.client.shopaapp.api.requests.ChangePasswordRequest;
import gr.shopa.client.shopaapp.api.requests.ProfileEditRequest;
import gr.shopa.client.shopaapp.api.requests.ProfileRequest;
import gr.shopa.client.shopaapp.api.requests.UserRequest;
import gr.shopa.client.shopaapp.api.responses.AuthResponse;
import gr.shopa.client.shopaapp.api.responses.CitiesResponse;
import gr.shopa.client.shopaapp.api.responses.OfferResponse;
import gr.shopa.client.shopaapp.api.responses.PointsResponse;
import gr.shopa.client.shopaapp.api.responses.ProfileResponse;
import gr.shopa.client.shopaapp.api.responses.PurchasesResponse;
import gr.shopa.client.shopaapp.api.responses.ShopResponse;
import gr.shopa.client.shopaapp.api.responses.UserResponse;
import rx.Observable;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Stathis on 03-Mar-18.
 */

public interface ShopaAPI {

    /** AUTHENTICATION --------------------------------------------------------------------------**/

    @FormUrlEncoded
    @POST("/oauth2/token")
    Call<AuthResponse> login(@Field("username") String username,
                             @Field("password") String password,
                             @Field("grant_type") String grant_type);

    /** ACCOUNTS --------------------------------------------------------------------------------**/

    @POST("/api/accounts/create")
    Call<UserRequest> createAccount(@Body UserRequest user );

    @POST("/api/accounts/ChangePassword")
    Call<ChangePasswordRequest> changePassword(@Body ChangePasswordRequest cpr);

    @DELETE("/api/accounts/user/{userId}")
    Call<UserResponse> deleteUser(@Path(value = "userId", encoded = true) String userId);

    @GET("/api/accounts/ReSendConfirmEmail?email={email}")
    Call<Response> resendConfirmEmail(@Path (value = "email", encoded = true) String email);

    /** BOOKMARKS -------------------------------------------------------------------------------**/

    @GET("/api/bookmarks/mine")
    Call<List<ShopResponse>> getMyBookmarks();

    @POST("/api/bookmarks/shop/{shopId}")
    Call<Request> addBookmark(@Path (value = "shopId", encoded = true) int shopId);

    @DELETE("/api/bookmarks/shop/{shopId}")
    Call<Request> removeBookmark(@Path (value = "shopId", encoded = true) int shopId);

    /** CITIES ----------------------------------------------------------------------------------**/

    @GET("/api/cities")
    Call<List<CitiesResponse>> getCities();

    /** INTERESTS -------------------------------------------------------------------------------**/

    @GET("/api/interests/mine")
    Call<List<OfferResponse>> getMyInterests();

    @POST("/api/interests/offer/{offerId}")
    Call<Request> addInterest(@Path (value = "offerId", encoded = true) int offerId);

    @DELETE("/api/interests/offer/{offerId}")
    Call<Request> removeInterest(@Path (value = "offerId", encoded = true) int offerId);

    /** USER & PROFILE --------------------------------------------------------------------------**/

    @GET("/api/accounts/user/current")
    Call<UserResponse> getCurrentUser();

    @GET("/api/profiles/mine")
    Call<ProfileResponse> getMyProfile();

    @POST("/api/profiles")
    Call<ProfileRequest> createProfile(@Body ProfileRequest profile);

    @PUT("/api/profiles/mine/update")
    Call<ProfileEditRequest> updateProfile(@Body ProfileEditRequest profile);

    /** OFFERS ----------------------------------------------------------------------------------**/

    /*@GET("/api/offers/client/mine")
    Call<List<OfferResponse>> getMyFeed();*/

    @GET("/api/offers/client/mine")
    Observable<List<OfferResponse>> getMyFeed();

    /** POINTS ----------------------------------------------------------------------------------**/

    @GET("/api/points/mine")
    Call<PointsResponse> getMyPoints();

    /** PURCHASES -------------------------------------------------------------------------------**/

    @GET("/api/purchases/mine")
    Call<List<PurchasesResponse>> getMyPurchases();

    /** SHOPS -------------------------------------------------------------------------------**/

    @GET("/api/shops/{shopId}")
    Call<ShopResponse> getShopById(@Path (value = "shopId", encoded = true) int shopId);

}
