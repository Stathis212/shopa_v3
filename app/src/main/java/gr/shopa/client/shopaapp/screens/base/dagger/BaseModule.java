package gr.shopa.client.shopaapp.screens.base.dagger;

import dagger.Module;
import dagger.Provides;
import gr.shopa.client.shopaapp.api.ShopaAPI;
import gr.shopa.client.shopaapp.screens.base.BaseActivity;
import gr.shopa.client.shopaapp.screens.base.core.BaseModel;
import gr.shopa.client.shopaapp.screens.base.core.BasePresenter;
import gr.shopa.client.shopaapp.screens.base.core.BaseView;
import gr.shopa.client.shopaapp.utils.rx.RxSchedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Module
public class BaseModule {

    BaseActivity baseContext;

    public BaseModule(BaseActivity context) {
        this.baseContext = context;
    }

    @BaseScope
    @Provides
    BaseActivity provideBaseContext() {
        return baseContext;
    }

    @BaseScope
    @Provides
    BasePresenter providePresenter(RxSchedulers schedulers, BaseModel model) {
        CompositeSubscription compositeSubscription = new CompositeSubscription();
        return new BasePresenter(model, schedulers, compositeSubscription);
    }

    @BaseScope
    @Provides
    BaseView provideBaseView(BaseActivity context) {
        return new BaseView(context);
    }

    @BaseScope
    @Provides
    BaseModel provideBaseModel(ShopaAPI api, BaseActivity ctx) {
        return new BaseModel(api, ctx);
    }

}
