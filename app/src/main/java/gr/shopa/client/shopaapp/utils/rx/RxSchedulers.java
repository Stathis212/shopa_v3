package gr.shopa.client.shopaapp.utils.rx;

import rx.Scheduler;

/**
 * Created by Stathis on 03-Mar-18.
 */

public interface RxSchedulers {
    Scheduler runOnBackground();

    Scheduler io();

    Scheduler compute();

    Scheduler androidThread();

    Scheduler internet();
}
