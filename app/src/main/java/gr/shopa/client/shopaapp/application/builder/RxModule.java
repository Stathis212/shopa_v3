package gr.shopa.client.shopaapp.application.builder;

import dagger.Module;
import dagger.Provides;
import gr.shopa.client.shopaapp.utils.rx.RxSchedulers;
import gr.shopa.client.shopaapp.utils.rx.ShopaRxSchedulers;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Module
public class RxModule {
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new ShopaRxSchedulers();
    }
}
