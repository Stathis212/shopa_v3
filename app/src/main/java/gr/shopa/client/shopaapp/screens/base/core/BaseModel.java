package gr.shopa.client.shopaapp.screens.base.core;

import java.util.List;

import gr.shopa.client.shopaapp.api.ShopaAPI;
import gr.shopa.client.shopaapp.api.responses.OfferResponse;
import gr.shopa.client.shopaapp.models.OfferModel;
import gr.shopa.client.shopaapp.screens.base.BaseActivity;
import gr.shopa.client.shopaapp.utils.NetworkUtils;
import rx.Observable;

/**
 * Created by Stathis on 04-Mar-18.
 */

public class BaseModel {

    private ShopaAPI api;
    private BaseActivity baseContext;

    public BaseModel(ShopaAPI api, BaseActivity baseCtx) {
        this.api = api;
        this.baseContext = baseCtx;

    }

    Observable<List<OfferResponse>> provideListOffers() {
        return api.getMyFeed();
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(baseContext);
    }


    public void gotoHeroesListActivity() {
        baseContext.showFeedListActivity();

    }

}
