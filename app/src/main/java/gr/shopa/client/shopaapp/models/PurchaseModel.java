package gr.shopa.client.shopaapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by stathis on 26-Feb-18.
 */

public class PurchaseModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("shopId")
    @Expose
    private Integer shopId;
    @SerializedName("shopName")
    @Expose
    private String shopName;
    @SerializedName("categories")
    @Expose
    private List<String> categories;
    @SerializedName("purchaseCategories")
    @Expose
    private Object purchaseCategories;

    public PurchaseModel(Integer id, Integer amount, String date, Integer shopId, String shopName, List<String> categories) {
        this.id = id;
        this.shopName = shopName;
        this.amount = amount;
        this.date = date;
        this.shopId = shopId;
        this.categories = categories;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public Object getPurchaseCategories() {
        return purchaseCategories;
    }

    public void setPurchaseCategories(Object purchaseCategories) {
        this.purchaseCategories = purchaseCategories;
    }
}
