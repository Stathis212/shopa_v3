package gr.shopa.client.shopaapp.screens.base.core;

import android.util.Log;

import gr.shopa.client.shopaapp.utils.UiUtils;
import gr.shopa.client.shopaapp.utils.rx.RxSchedulers;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Stathis on 04-Mar-18.
 */

public class BasePresenter {

    private BaseModel model;
    private RxSchedulers rxSchedulers;
    private CompositeSubscription subscriptions;


    public BasePresenter(BaseModel model, RxSchedulers schedulers, CompositeSubscription subscriptions) {
        this.model = model;
        this.rxSchedulers = schedulers;
        this.subscriptions = subscriptions;
    }


    public void onCreate() {
        subscriptions.add(getFeedList());
    }

    public void onDestroy() {
        subscriptions.clear();
    }

    private Subscription getFeedList() {

        return model.isNetworkAvailable().doOnNext(networkAvailable -> {
            if (!networkAvailable) {
                Log.d("no conn", "no connection");
            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.isNetworkAvailable()).
                subscribeOn(rxSchedulers.internet()).
                observeOn(rxSchedulers.androidThread()).subscribe(aBoolean -> model.gotoHeroesListActivity(), throwable -> UiUtils.handleThrowable(throwable));
    }


}
