package gr.shopa.client.shopaapp.screens.base;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import gr.shopa.client.shopaapp.R;
import gr.shopa.client.shopaapp.application.ShopaController;
import gr.shopa.client.shopaapp.application.builder.ShopaModule;
import gr.shopa.client.shopaapp.screens.base.core.BasePresenter;
import gr.shopa.client.shopaapp.screens.base.core.BaseView;
import gr.shopa.client.shopaapp.screens.base.dagger.BaseModule;
import gr.shopa.client.shopaapp.screens.base.dagger.DaggerBaseComponent;

public class BaseActivity extends AppCompatActivity {

    @Inject
    BaseView view;
    @Inject
    BasePresenter basePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerBaseComponent.builder().
                shopaComponent(ShopaController.shopaComponent()).
                baseModule(new BaseModule(this)).
                build().inject(this);

        setContentView(R.layout.activity_base);
        basePresenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        basePresenter.onDestroy();
    }

    public void showFeedListActivity() {
        Log.d("loaded", "ok showed");
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

}
