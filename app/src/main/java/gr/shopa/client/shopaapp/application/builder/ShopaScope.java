package gr.shopa.client.shopaapp.application.builder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ShopaScope {
}