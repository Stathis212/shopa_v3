package gr.shopa.client.shopaapp.application.builder;

import android.content.Context;

import com.google.common.net.HttpHeaders;

import java.io.File;
import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import gr.shopa.client.shopaapp.utils.rx.ShopaRxSchedulers;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Module
public class NetworkModule {

    @ShopaScope
    @Provides
    OkHttpClient provideHttpClient(HttpLoggingInterceptor logger, Cache cache, String token) {

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request();

                if (token != null && !token.isEmpty()){
                    Request.Builder finalRequest = newRequest.newBuilder().addHeader("Authorization", "Bearer " + token);
                    return chain.proceed(finalRequest.build());
                }else{
                    return chain.proceed(newRequest.newBuilder().build());
                }
            }
        });
        builder.addInterceptor(logger);
        builder.cache(cache);
        return builder.build();
    }

    @ShopaScope
    @Provides
    HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @ShopaScope
    @Provides
    Cache provideCache(File file) {
        return new Cache(file, 10 * 10 * 1000);
    }

    @ShopaScope
    @Provides
    File provideCacheFile(Context context) {
        return context.getFilesDir();
    }

    @ShopaScope
    @Provides
    RxJavaCallAdapterFactory provideRxAdapter() {
        return RxJavaCallAdapterFactory.createWithScheduler(ShopaRxSchedulers.INTERNET_SCHEDULERS);
    }


    @Provides
    GsonConverterFactory provideGsonClient() {
        return GsonConverterFactory.create();
    }

}