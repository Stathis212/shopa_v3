package gr.shopa.client.shopaapp.application.builder;

import dagger.Component;
import gr.shopa.client.shopaapp.api.ShopaAPI;
import gr.shopa.client.shopaapp.utils.rx.RxSchedulers;

/**
 * Created by Stathis on 03-Mar-18.
 */

@ShopaScope
@Component(modules = {ShopaModule.class, NetworkModule.class, RxModule.class, ServiceModule.class})
public interface ShopaComponent {
    RxSchedulers rxSchedulers();
    ShopaAPI apiService();
}
