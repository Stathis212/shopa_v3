package gr.shopa.client.shopaapp.application.builder;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Stathis on 03-Mar-18.
 */

@Module
public class ShopaModule {

    private final Context context;

    public ShopaModule(Context aContext) {
        this.context = aContext;
    }

    @ShopaScope
    @Provides
    Context provideAppContext() {
        return context;
    }
}
